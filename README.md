# kms-apiv1-js

Exemplo de uso da API KMS desenvolvida pela Bry Tecnologia.
Implementado em JavaScript com auxílio da biblioteca [Axios](https://github.com/axios/axios).

## Instruções de instalação

No terminal digite os seguintes comandos:

```Shell Script
git clone https://git.bry.com.br/kms/api/exemplos/kms-apiv1-js.git

```

```Shell Script
cd kms-apiv1-js
```

```Shell Script
npm install
```

## Instruções de uso

Atente ao arquivo `cliente.js`. Com exceção das chamadas de listagem, você terá que configurar as chamadas para realizar a operação desejada, tendo em mente o tipo de compartimento que você deseja manipular e os tipos de chave.  
Para maiores detalhes de como realizar as chamadas [clique aqui](https://kms.bry.com.br/kms-api/).

## Exemplos

1- Para criar um compartimento do tipo PIN, você terá que chamar a função tipoCompartimento.pin(pin, puk), onde 'pin' e 'puk' serão do formato string.

```JavaScript
const geradorCompartimento = async () => {
    try {
        instance.defaults.headers.common['Authorization'] = `Bearer ${await emiteJwt()}`
        const { data } = await instance.post('/compartimentos', null, tipoCompartimento.pin('seuPin', 'seuPuk'))
        console.log(data)
    } catch (error) {
        console.error(error)
    }
}
```

2- Para criar um compartimento OTP o processo é muito parecido ao do tipo PIN porém você passará apenas o puk como argumento. Também é preciso copiar e colar o QR Code no arquivo `qrCode.html`. Você vai obter o valor do QR Code em base64 na resposta da sua chamada.

```JavaScript
const geradorCompartimento = async () => {
    try {
        instance.defaults.headers.common['Authorization'] = `Bearer ${await emiteJwt()}`
        const { data } = await instance.post('/compartimentos', null, tipoCompartimento.otp('seuPuk'))
        console.log(data)
    } catch (error) {
        console.error(error)
    }
}
```

3- Para criar uma chave AC você terá que chamar a função tipoChave.ac(tipo, senha), onde 'tipo' será PIN ou OTP em formato string e 'senha' será a senha do seu compartimento PIN ou a senha gerada por celular do compartimente OTP.

```JavaScript
const geradorChave = async (uuid_compartimento) => {
    try {
        instance.defaults.headers.common['Authorization'] = `Bearer ${await emiteJwt()}`
        const { data } = await instance.post(`/compartimentos/${uuid_compartimento}/chaves`, null, tipoChave.ac('PIN', 'seuPin'))
        console.log(data)
    } catch (error) {
        console.error(error)
    }
}
```

4- Neste exemplo existem duas formas de assinar, tipoAssinatura.pin(senha) e tipoAssinatura.otp().  
A solicitação OTP envia uma notificação push para o celular vinculado e você autoriza ou não a assinatura. Caso você autorize, a assinatura vai ser realizada automaticamente.  

```JavaScript
const solicitacaoAssinatura = async (uuid_chave) => {
    try {
        instance.defaults.headers.common['Authorization'] = `Bearer ${await emiteJwt()}`
        const { data } = await instance.post(`/chaves/${uuid_chave}/assinaturas`, null, tipoAssinatura.otp())
        console.log(data)
    } catch (error) {
        console.error(error)
    }
}
```

A solicitação PIN exige apenas sua senha PIN em formato string e já realiza a assinatura.

```JavaScript
const solicitacaoAssinatura = async (uuid_chave) => {
    try {
        instance.defaults.headers.common['Authorization'] = `Bearer ${await emiteJwt()}`
        const { data } = await instance.post(`/chaves/${uuid_chave}/assinaturas`, null, tipoAssinatura.pin('seuPin'))
        console.log(data)
    } catch (error) {
        console.error(error)
    }
}
```

## Por fim

Para rodar os testes basta estar no diretório src e digitar no terminal o seguinte comando:

```Shell Script
node cliente.js
```
