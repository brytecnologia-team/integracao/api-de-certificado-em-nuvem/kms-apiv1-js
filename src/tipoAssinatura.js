const pin = (senha) => {
    senha = Buffer.from(senha).toString('base64')
    const config = {
        headers: {
            kms_credencial: senha,
            kms_credencial_tipo: 'PIN'
        },
        data: {
            aplicacao: 'Testando',
            descricao: 'Testando',
            hashes: [
                {
                    algoritmo: 'SHA256',
                    valores: [
                        'MDEyMzQ1Njc4OTAxMjM0NTY3ODkwMTIzNDU0Mjg5MTI=' // string de 256 bits (32 caractéres) convertida em base64
                    ]
                }
            ],
            codificarAlgoritmo: false
        }
    }
    return config
}

const otp = () => {
    const config = {
        params: {
            acao: 'aguardar'
        },
        data: {
            aplicacao: 'TesteOtpAuto',
            descricao: 'TesteOtpAuto',
            hashes: [
                {
                    algoritmo: 'SHA256',
                    valores: [
                        'MDEyMzQ1Njc4OTAxMjM0NTY3ODkwMTIzNDU0Mjg5MTI=' // string de 256 bits (32 caractéres) convertida em base64
                    ]
                }
            ],
            codificarAlgoritmo: false
        }
    }
    return config
}

module.exports = { pin, otp }