const axios = require('axios')
const qs = require('querystring')
const tipoCompartimento = require('./tipoCompartimento')
const tipoChave = require('./tipoChave')
const tipoAssinatura = require('./tipoAssinatura')

const instance = axios.create({
    baseURL: 'https://kms.hom.bry.com.br/kms/rest/v1',
    headers: {
        'Content-Type': 'application/json'
    }
})

const emiteJwt = async () => {
    const body = {
        grant_type: "client_credentials",
        client_id: "25398d93-12f0-401c-9310-c8810e3c7836",
        client_secret: "Q4J85GprqYf16U2uYQdpMHTt4zx6hJessmHfTtrC2s/XkrNn3Jfz7A=="
    }
    const config = { 'Content-Type': 'application/x-www-form-urlencoded' }
    const { data } = await axios.post('https://kms.hom.bry.com.br/token-service/jwt', qs.stringify(body), config)
    return data.access_token
}

const geradorCompartimento = async () => {
    try {
        instance.defaults.headers.common['Authorization'] = `Bearer ${await emiteJwt()}`
        const { data } = await instance.post('/compartimentos', null, tipoCompartimento.pin('bry123', 'bry123'))
        console.log(data)
    } catch (error) {
        console.error(error)
    }
}

const geradorChave = async (uuid_compartimento) => {
    try {
        instance.defaults.headers.common['Authorization'] = `Bearer ${await emiteJwt()}`
        const { data } = await instance.post(`/compartimentos/${uuid_compartimento}/chaves`, null, tipoChave.importar('PIN', 'bry123'))
        console.log(data)
    } catch (error) {
        console.error(error)
    }
}

const solicitacaoAssinatura = async (uuid_chave) => {
    try {
        instance.defaults.headers.common['Authorization'] = `Bearer ${await emiteJwt()}`
        const { data } = await instance.post(`/chaves/${uuid_chave}/assinaturas`, null, tipoAssinatura.pin('bry123'))
        console.log(data)
    } catch (error) {
        console.error(error)
    }
}

// descomente a parte a ser testada
const rodar = async () => {
    const uuid_compartimento = 'd680421c-a1f3-48bc-9c38-f018df017e80'
    const uuid_chave = 'b3930d73-f360-4d49-8486-8a691d2827f4'

    // await geradorCompartimento()
    // await geradorChave(uuid_compartimento)
    // await solicitacaoAssinatura(uuid_chave)
}
rodar()