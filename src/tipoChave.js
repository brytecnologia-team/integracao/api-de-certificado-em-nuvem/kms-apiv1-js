const idAleatorio = () => {
    const num = Math.floor(Math.random() * (1000000 - 1) + 1)
    return String(num)
}

const ac = (tipo, senha) => {
    if (tipo === 'PIN') senha = Buffer.from(senha).toString('base64')
    const config = {
        params: {
            modo: 'ac'
        },
        headers: {
            kms_credencial: senha,
            kms_credencial_tipo: tipo
        },
        data: {
            identificador: idAleatorio(),
            algoritmo: "RSA",
            tamanho: "2048",
            dadosTitular: {
                nome: "Fulano do Teste",
                email: "fulano@bry.com.br",
                cpf: "12345678909",
                rg: "41156714",
                emissorRg: "SSP/SC",
                dataNascimento: "10/10/1990",
                enderecoResidencial: {
                    rua: "Lauro Linhares",
                    bairro: "Trindade",
                    cep: "88036002",
                    cidade: "Florianopolis",
                    estado: "SC",
                    pais: "BR"
                },
                telResidencial: {
                    ddi: "+55",
                    ddd: "48",
                    telefone: "32346696"
                }
            },
            identificadorAC: "BRYV2"
        }
    }
    return config
}

const importar = (tipo, senha) => {
    if (tipo === 'PIN') senha = Buffer.from(senha).toString('base64')

    const fileSystem = require('fs')
    const certificado = fileSystem.readFileSync('../certificadoParaImportacao/base64Certificado.txt', 'utf8')
    const certificadoString = certificado.toString()

    const config = {
        params: {
            modo: 'importar'
        },
        headers: {
            kms_credencial: senha,
            kms_credencial_tipo: tipo
        },
        data: {
            identificador: idAleatorio(),
            pkcs12: {
                dados: certificadoString,
                senha: 'bry123',
                senhaCifrada: false
            }
        }
    }
    return config
}

module.exports = { ac, importar }