const pin = (pin, puk) => {
    const valorPin = Buffer.from(pin).toString('base64')
    const valorPuk = Buffer.from(puk).toString('base64')
    const config = {
        data: {
            tipo: 'PIN',
            pin: valorPin,
            puk: valorPuk
        }
    }
    return config
}

const otp = (puk) => {
    valorPuk = Buffer.from(puk).toString('base64')
    const config = {
        data: {
            tipo: 'OTP',
            puk: valorPuk
        }
    }
    return config
}

module.exports = { pin, otp }